# Kubernetes

## Resolução

1º Para criar um cluster local de Kubernetes instalei o minikube na versão v1.23.2 e o helm v3.7.0. 

2º Para que o HELM identificasse e utilizasse uma imagem do docker local inseri no script um comando para que ele usasse desse modo.

3º Para usar o ingress foi necessário habilitar o addons no minikube e adicionar no arquivo hosts da minha máquina para resolver o hostname que eu defini no ingress (na execução do script isso já é feito).

4º Para rodar o minikube, o comando para que o helm identificasse o repositorio local e etc, criei um script chamado k8s.sh, ao roda-lo todo nosso cluster será criado.

5º Todos as atividades extras foram criadas o health check pelo deployment e a utilização da variavel **$NAME** para alterar o texto de exibição da aplicação.

## Vamos lá!!!

Requisitos:
> Ubuntu (testado na versão 18.04) -
> Minikube 1.23.2 -
> Helm v3.7.0

Clone o repositório:
> git clone https://gitlab.com/amonteirojr58/desafio-newway.git

Entre na pasta do desafio:
> cd desafio-newway/kubernetes/

Execute o comando abaixo, isso vai dar permissão de execução ao nosso script.
> chmod +x k8s.sh

Execute o comando abaixo, vai fazer todas as instalações necessárias e subir o cluster.
> ./k8s.sh


**Agora acesse a aplicação pelo browser ou faça um curl em http://newway.desafio**.

**Detalhes importantes:** 
- O cluster está subindo o minikube pelo virtualbox que é o padrão.
- Toda criação e testes foram feitos no Ubuntu 18.04 e é preciso rodar o script com privilegios de administrador.
- Todo deploy está sendo feito em um namespace que eu criei chamado **newway-desafio**.
