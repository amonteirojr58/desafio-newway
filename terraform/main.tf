provider "aws" {
  region     = "us-east-1"
}


module "app" {
  source = "./modules/app"
  newway = module.networking.newway
  pub_subnet = module.networking.pub_subnet
  ip = var.meuip
}

module "networking"  {
  source = "./modules/networking"
  ip = var.meuip
}

