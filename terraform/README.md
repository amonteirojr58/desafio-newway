# Infrastructure-as-code - Terraform

## Resolução

1º Fiz a instalação do terraform na versão v1.0.5, criei os modulos, divididos em app e networking. 

2º Depois criei o arquivo _(variables.tf)_ de rede e região.

3º Fiz a criação do outpots.tf no main, para que mostrasse ao final do apply o IP publico da instancia criada.

4º Ao rodar o comando _terraform apply_ será solicitado o IP de saida do usuario que rodou.

5º Ao rodar o terraform para criação, favor exportar as chaves de login da AWS (AWS_ACCESS_KEY e AWS_SECRET_ACCESS_KEY)

6º Utilizei a função user_data para renderizar o arquivo _install.sh_ que instala o docker e criar o container do apache.

Segue o passo a passo da execução.

## Vamos lá!!!

Requisitos:
> Terraform v1.0.5+

- Em posse das crendencias de acesso criadas no console da AWS, exportar as variaveis com o comando _aws configure_ (AWS_ACCESS_KEY_ID e a AWS_SECRET_ACCESS_KEY) com os valores correspondente a criação do console.

- será necessario a criação de uma key pair para EC2. (Armazene pois será utilizada pelo terraform)

- Abra o arquivo no diretório _terraform/modules/app/ec2.tf_ e insira o nome da key pair criada, removendo o campo comentado **key_name**. Assim terá acesso ssh a instancia que será criada.

- Caso não esteja, volte na pasta raiz do teste **terraform** e execute os comandos abaixo:

> terraform init
 
> terraform plan

Após a validação do código, rodar o comando a seguir para iniciar a criação
> terraform apply --auto-approve

- Finalizado, copie o <public_ip> gerado no _Outputs_ e em alguns segundos teste acessando o browser do seu computador ou realizando um _curl_, tem que aparecer o html padrão do apache, conforme solicitado no teste.

- Caso tenha feito todas as criações e configurações das etapas informadas o acesso ssh vai estar funcionando.

- Lembre de dar permissão no key pair.
> chmod 400 suakeypair.pem 
- Faça o acesso.
> ssh -i "suakeypair.pem" ubuntu@<public_ip>

Obrigado
