output "pub_subnet" {
  value = aws_subnet.pub_subnet.id
}

output "newway" {
  value = aws_security_group.newway.id
}

#output "ip" {
#  value = aws_security_group.newway.cidr_blocks
#}

