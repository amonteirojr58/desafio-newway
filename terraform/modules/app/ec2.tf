data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "template_file" "user_data" {
  template = file("./install.sh")
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id = var.pub_subnet
  security_groups = [var.newway]
  ##inserir o nome da chave no campo abaixo e descomentar a linha##
  ##key_name = ""
  user_data = data.template_file.user_data.rendered


  tags = {
    Name = "New-way"
  }


}
